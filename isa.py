from enum import Enum

import json


class Opcode(str, Enum):
    LD = 'ld'
    ST = 'st'
    ADD = 'add'
    SUB = 'sub'
    JMP = 'jmp'
    JE = 'je'
    JNE = 'jne'
    DIV = 'div'
    MOD = 'mod'
    INC = 'inc'
    DEC = 'dec'
    HLT = 'hlt'
    IN = 'in'
    OUT = 'out'
    CMP = 'cmp'
    EI = 'ei'
    DI = 'di'
    IRET = 'iret'


class AddressingType(str, Enum):
    CONSTANT = 'constant',
    DIRECT_ADDRESSING = 'direct_addressing',
    INDIRECT_ADDRESSING = 'indirect_addressing'


number_of_args = {
    Opcode.LD.name: 1,
    Opcode.ST.name: 1,
    Opcode.ADD.name: 1,
    Opcode.SUB.name: 1,
    Opcode.JMP.name: 1,
    Opcode.JE.name: 1,
    Opcode.JNE.name: 1,
    Opcode.DIV.name: 1,
    Opcode.MOD.name: 1,
    Opcode.INC.name: 0,
    Opcode.DEC.name: 0,
    Opcode.HLT.name: 0,
    Opcode.IN.name: 0,
    Opcode.OUT.name: 0,
    Opcode.CMP.name: 1,
    Opcode.EI.name: 0,
    Opcode.DI.name: 0,
    Opcode.IRET.name: 0
}


def write_code(filename, code):
    with open(filename, 'w', encoding='utf-8') as file:
        file.write(json.dumps(code, indent=4))


def read_code(filename):
    with open(filename, 'r', encoding='utf-8') as file:
        code = json.loads(file.read())
    for instr in code['text']:
        instr['opcode'] = Opcode(instr['opcode'])
        if instr['addressing_type']:
            instr['addressing_type'] = AddressingType(instr['addressing_type'])
    return code


