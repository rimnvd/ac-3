import pytest
import logging
import os
import tempfile
import io
import contextlib
import unittest

import translator
import machine

@pytest.mark.golden_test('golden/*.yml')
def test_whole_by_golden(golden, caplog):
    caplog.set_level(logging.DEBUG)

    with tempfile.TemporaryDirectory() as tmpdirname:
        source = os.path.join(tmpdirname, "source")
        input_stream = os.path.join(tmpdirname, "input.txt")
        target = os.path.join(tmpdirname, "target.out")
        with open(source, 'w', encoding='utf-8') as file:
            file.write(golden['source'])
        with open(input_stream, "w", encoding="utf-8") as file:
            file.write(golden['input'])
        with contextlib.redirect_stdout(io.StringIO()) as stdout:
            translator.main([source, target])
            machine.main([target, input_stream])
        with open(target, encoding="utf-8") as file:
            code = file.read()

    assert code == golden.out["code"]
    assert stdout.getvalue() == golden.out["output"]

class TestTranslator(unittest.TestCase):

    def test_hello(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "examples/hello"
            target = os.path.join(tmpdirname, "hello.out")
            input_stream = "examples/hello_input.txt"

            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                translator.main([source, target])
                machine.main([target, input_stream])
            self.assertEqual(stdout.getvalue(),
                             'source LoC: 16 code instr: 26\nhello, world!\ninstr_counter: 107 ticks: 148\n')

    def test_cat(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "examples/cat"
            target = os.path.join(tmpdirname, "cat.out")
            input_stream = "examples/cat_input.txt"

            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                translator.main([source, target])
                machine.main([target, input_stream])

            self.assertEqual(stdout.getvalue(),
                             'source LoC: 16 code instr: 10\nhello\ninstr_counter: 39 ticks: 46\n')

    def test_prob1(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "examples/prob1"
            target = os.path.join(tmpdirname, "prob1")
            input_stream = "examples/prob1_input.txt"

            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                translator.main([source, target])
                machine.main([target, input_stream])

            self.assertEqual(stdout.getvalue(),
                             'source LoC: 32 code instr: 28\n233168\ninstr_counter: 14590 ticks: 22984\n')
