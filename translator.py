import re, sys

from typing import Tuple
from isa import write_code, number_of_args, Opcode, AddressingType
from collections import defaultdict

LABEL = r'\.[a-zA-Z_][a-zA-Z_\d]*:'


def clean_code(lst_of_text: list[str]):
    code = list(map(lambda x: x.split(';')[0], lst_of_text))
    code = list(map(lambda x: x.strip(), code))
    code = list(filter(bool, code))
    code = list(map(lambda x: re.sub(r'\s+', ' ', x), code))
    return code


def parse_vector(vector_list: list[str], label_dict: dict) -> int:
    assert len(vector_list) == 1, 'vector section format is wrong'
    vector_str = vector_list[0]
    assert re.fullmatch(r'[a-zA-Z][a-zA-Z\d_]* num .[a-zA-Z_][a-zA-Z_\d]*',
                        vector_str), 'vector section format is wrong'
    label = vector_str.split()[-1]
    assert label in label_dict, f'label not found, {label}'
    return label_dict[label]


def parse_data(data_list: list[str]) -> Tuple[defaultdict, dict, list]:
    assert not (any(map(lambda x: re.match(LABEL, x), data_list)))
    d = defaultdict(list)
    variables = dict()
    data_type_list = [None, None, None]
    current_address = 3  # 0 - input port, 1 - output port, 2 - interrupt vector
    for elem in data_list:
        value = elem.split(maxsplit=2)
        if re.fullmatch(r'[a-zA-Z][a-zA-Z\d_]* num -?\d+', elem):
            assert value[0] not in variables, 'already exist'
            variables[value[0]] = current_address
            current_address += 1
            d['data'].append(int(value[-1]))
            data_type_list.append(0)
        elif re.fullmatch(r'[a-zA-Z][a-zA-Z\d_]* string ".+"', elem):
            assert value[0] not in variables, 'already exist'
            variables[value[0]] = current_address
            string = value[-1][1:-1:]
            current_address += len(string)
            for char in string:
                d['data'].append(ord(char))
            data_type_list.extend([1] * len(string))
        else:
            assert False, 'data section is empty or format is wrong'
    return d, variables, data_type_list


def replace_label(label_to_replace_list: list[str], label_dict: dict, instr_list: list[dict]):
    for label in label_to_replace_list:
        assert label in label_dict, f'label not found, {label}'
        for instr_dict in instr_list:
            if instr_dict['arg'] == label:
                instr_dict['arg'] = label_dict[label]


def parse_text(text_list: list[str], variables: dict, data_type_list: list, data: list) -> Tuple[list[dict], dict]:
    label_dict = dict()
    label_to_replace_list = []
    instr_list = []
    instr_number = 0
    out_type_arg = 0 # 0 - num, 1 - string
    for elem in text_list:
        if re.fullmatch(LABEL, elem):
            assert elem[:-1] not in label_dict, f'label {elem[:-1]} already exist'
            label_dict[elem[:-1]] = instr_number
        else:
            tokens = elem.split()
            instruction = tokens[0].upper()
            assert instruction in number_of_args, 'instruction not found'
            assert len(tokens) - 1 == number_of_args[instruction]
            if len(tokens) == 1:
                if instruction == 'OUT':
                    instr_list.append({'opcode': Opcode[instruction],
                                       'arg': out_type_arg,
                                       'addressing_type': None})
                elif instruction == 'IN':
                    out_type_arg = 1
                    instr_list.append({'opcode': Opcode[instruction],
                                       'arg': None,
                                       'addressing_type': None})
                else:
                    instr_list.append({'opcode': Opcode[instruction],
                                       'arg': None,
                                       'addressing_type': None})
            else:
                arg = tokens[1]
                if re.fullmatch(r'#-?\d+', arg):
                    assert instruction not in ['JE', 'JNE', 'JMP', 'ST'], 'illegal argument'
                    instr_list.append({'opcode': Opcode[instruction],
                                       'arg': int(arg[1:]),
                                       'addressing_type': AddressingType['CONSTANT']})
                    if instruction == 'LD':
                        out_type_arg = 0
                elif re.fullmatch(r'\d+', arg):
                    assert instruction not in ['JE', 'JNE', 'JMP'], 'illegal argument'
                    instr_list.append({'opcode': Opcode[instruction],
                                       'arg': arg,
                                       'addressing_type': AddressingType['DIRECT_ADDRESSING']})
                    if instruction == 'LD':
                        out_type_arg = data_type_list[int(arg)]
                elif re.fullmatch(r'[a-zA-Z][a-zA-Z\d_]*', arg):
                    assert instruction not in ['JE', 'JNE', 'JMP'], 'illegal argument'
                    assert arg in variables, 'argument not found'
                    instr_list.append({'opcode': Opcode[instruction],
                                       'arg': variables[arg],
                                       'addressing_type': AddressingType['DIRECT_ADDRESSING']})
                    if instruction == 'LD':
                        out_type_arg = data_type_list[variables[arg] - 3]
                elif re.fullmatch(r'\([a-zA-Z][a-zA-Z\d_]*\)', arg):
                    assert instruction not in ['JE', 'JNE', 'JMP'], 'illegal argument'
                    arg = arg[1:-1]
                    assert arg in variables, 'argument not found'
                    instr_list.append({'opcode': Opcode[instruction],
                                       'arg': variables[arg],
                                       'addressing_type': AddressingType['INDIRECT_ADDRESSING']})
                    if instruction == 'LD':
                        out_type_arg = data_type_list[data[variables[arg] - 3]]
                elif re.fullmatch(r'\.[a-zA-Z_][a-zA-Z_\d]*', arg):
                    assert instruction in ['JE', 'JNE', 'JMP'], 'illegal argument'
                    if arg in label_dict:
                        instr_list.append({'opcode': Opcode[instruction],
                                           'arg': label_dict[arg],
                                           'addressing_type': AddressingType['CONSTANT']})
                    else:
                        label_to_replace_list.append(arg)
                        instr_list.append({'opcode': Opcode[instruction],
                                           'arg': arg,
                                           'addressing_type': AddressingType['CONSTANT']})
                else:
                    assert False, 'illegal argument'
            instr_number += 1
    replace_label(label_to_replace_list, label_dict, instr_list)
    return instr_list, label_dict


def main(args):
    assert len(args) == 2, \
        "Wrong arguments: translator.py <input_file> <target_file>"
    source, target = args
    lst = []
    with open(source, "rt", encoding="utf-8") as f:
        for line in f:
            lst.append(line)
    code = clean_code(lst)
    count_instr = len(code)
    assert code.count('.data:') != 0, 'no data section in the code'
    assert code.count('.text:') != 0, 'no text section in the code'
    assert code.count('.data:') == 1, 'there can be only one data section in the code'
    assert code.count('.text:') == 1, 'there can be only one text section in the code'
    index_data = code.index('.data:')
    index_text = code.index('.text:')
    if '.vector:' in code:
        index_vector = code.index('.vector:')
        assert index_text > index_vector > index_data, 'wrong section order'
        data_section = code[index_data + 1:index_vector:]
        vector_section = code[index_vector + 1:index_text:]
        text_section = code[index_text + 1:]
        code, variables, data_type_list = parse_data(data_section)
        code['text'], label_dict = parse_text(text_section, variables, data_type_list, code['data'])
        code['data'].insert(0, int(parse_vector(vector_section, label_dict)))
    else:
        assert index_text > index_data, 'text section before data section'
        data_section = code[index_data + 1:index_text:]
        text_section = code[index_text + 1:]
        code, variables, data_type_list = parse_data(data_section)
        code['text'], label_dict = parse_text(text_section, variables, data_type_list, code['data'])
        code['text'].append(({'opcode': Opcode['IRET'],
                              'arg': None,
                              'addressing_type': None}))
        code['data'].insert(0, len(code['text']) - 1)
    print("source LoC:", count_instr, "code instr:", len(code['data']) + len(code['text']))
    write_code(target, code)


if __name__ == '__main__':
    main(sys.argv[1:])
