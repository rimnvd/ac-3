import logging
import sys

from isa import Opcode, AddressingType, read_code
from enum import Enum

class State(Enum):
    MAIN_PROGRAM = 0,
    INTERRUPT = 1

class DataPath:
    def __init__(self, data_memory_size: int, data_memory_init_state, input_buffer: list[tuple]):
        assert data_memory_size > 0, 'data memory size should be positive'
        assert len(data_memory_init_state) <= data_memory_size - 2, \
            'data size should be less or equal to data memory size'
        self.data_memory = [0] * data_memory_size
        self.zero_flag = True
        self.neg_flag = False
        self.acc = 0
        self.data_register = 0
        self.address_register = 0
        self.min_value = -2 ** 31
        self.max_value = 2 ** 31 - 1
        self.input_buffer = input_buffer
        self.output_buffer = []

        for i, val in enumerate(data_memory_init_state):
            self.data_memory[i + 2] = val

    def set_flags(self):
        self.zero_flag = False
        if self.acc == 0:
            self.zero_flag = True
        if self.acc < 0:
            self.neg_flag = True

    def set_cmp_flags(self, value):
        if value == 0:
            self.zero_flag = True
        if value < 0:
            self.neg_flag = True

    def is_zero(self) -> bool:
        return self.zero_flag

    def is_neg(self) -> bool:
        return self.neg_flag

    def latch_data_memory(self):
        self.data_memory[self.address_register] = self.acc

    def latch_data_register(self):
        self.data_register = self.data_memory[self.address_register]

    def put_to_data_register(self, value: int):
        assert self.min_value <= value <= self.max_value, 'value should be in the range [-2 ^ 31; 2 ^ 31 - 1] (int)'
        self.data_register = value

    def set_address_register(self, value):
        self.address_register = value

    def latch_acc(self):
        self.acc = self.data_register

    def output(self, value_type):
        if value_type:
            symbol = chr(self.acc)
        else:
            symbol = self.acc
        logging.debug('output: %s << %s', repr(
            ''.join(self.output_buffer)), repr(symbol))
        self.output_buffer.append(str(symbol))

    def input(self):
            ch = self.input_buffer.pop(0)[1]
            self.data_memory[0] = ord(ch)

    def alu(self, opcode):
        if opcode is Opcode.ADD:
            self.acc += self.data_register
        elif opcode is Opcode.SUB:
            self.acc -= self.data_register
        elif opcode is Opcode.DIV:
            self.acc //= self.data_register
        elif opcode is Opcode.MOD:
            self.acc %= self.data_register
        elif opcode is Opcode.INC:
            self.acc += 1
        elif opcode is Opcode.DEC:
            self.acc -= 1
        if self.acc > self.max_value:
            self.acc = self.acc - self.max_value + self.min_value - 1
        if self.acc < self.min_value:
            self.acc = self.acc - self.min_value + self.max_value + 1
        self.set_flags()


class ControlUnit:
    def __init__(self, code, data_path):
        self.code = code
        self.data_path = data_path
        self._tick = 0
        self.instruction_pointer = 0
        self.interrupt_enable = False
        self.context = tuple()
        self.ready_flag = True
        self.state = State.MAIN_PROGRAM

    def tick(self):
        self._tick += 1

    def current_tick(self):
        return self._tick

    def save_context(self):
        self.context = (self.data_path.acc, self.data_path.data_register, self.data_path.address_register,
                        self.data_path.zero_flag, self.data_path.neg_flag, self.instruction_pointer)

    def retrieve_context(self):
        self.data_path.acc, self.data_path.data_register, self.data_path.address_register, self.data_path.zero_flag,\
            self.data_path.neg_flag, self.instruction_pointer = self.context
        self.state = State.MAIN_PROGRAM
        self.ready()

    def is_ready(self):
        return self.ready_flag

    def ready(self):
        self.ready_flag = True

    def unready(self):
        self.ready_flag = False

    def get_arg(self, instruction):
        arg = instruction['arg']
        addressing_type = instruction['addressing_type']
        if addressing_type is AddressingType.CONSTANT:
            self.data_path.put_to_data_register(arg)
            return arg
        if addressing_type is AddressingType.DIRECT_ADDRESSING:
            self.data_path.set_address_register(arg)
            self.data_path.latch_data_register()
            self.tick()
            return self.data_path.data_register
        if addressing_type is AddressingType.INDIRECT_ADDRESSING:
            self.data_path.set_address_register(arg)
            self.data_path.latch_data_register()
            self.tick()
            self.data_path.set_address_register(self.data_path.data_register)
            self.data_path.latch_data_register()
            self.tick()
            return self.data_path.data_register


    def get_st_arg(self, instruction):
        arg = instruction['arg']
        addressing_type = instruction['addressing_type']
        if addressing_type is AddressingType.DIRECT_ADDRESSING:
            return arg
        if addressing_type is AddressingType.INDIRECT_ADDRESSING:
            self.data_path.set_address_register(arg)
            self.data_path.latch_data_register()
            self.tick()
            return self.data_path.data_register

    def latch_instruction_pointer(self, sel_next):
        if sel_next:
            self.instruction_pointer += 1
        else:
            instruction = self.code[self.instruction_pointer]
            arg = self.get_arg(instruction)
            self.instruction_pointer = arg

    def check_interrupt(self):
        if not self.interrupt_enable or len(self.data_path.input_buffer) == 0:
            return
        if self.current_tick() >= self.data_path.input_buffer[0][0] and self.is_ready():
            self.state = State.INTERRUPT
            self.do_interrupt()

    def do_interrupt(self):
        self.unready()
        self.save_context()
        self.data_path.set_address_register(2)
        self.instruction_pointer = self.data_path.latch_data_register()
        self.instruction_pointer = self.data_path.data_register
        self.tick()
        logging.debug(f'{{STATE: {self.state.INTERRUPT.name}}}')

    def decode_and_execute_instr(self):
        instruction = self.code[self.instruction_pointer]
        opcode = instruction['opcode']
        logging.debug('{%s started}', opcode)
        if opcode is Opcode.HLT:
            raise StopIteration()
        if opcode == Opcode.LD:
            self.get_arg(instruction)
            self.data_path.latch_acc()
            self.data_path.set_flags()
            self.latch_instruction_pointer(sel_next=True)
            self.tick()
        elif opcode is Opcode.ST:
            address = self.get_st_arg(instruction)
            self.data_path.set_address_register(address)
            self.data_path.latch_data_memory()
            self.latch_instruction_pointer(sel_next=True)
            self.tick()
        elif opcode in {Opcode.INC, Opcode.DEC}:
            self.data_path.alu(opcode)
            self.latch_instruction_pointer(sel_next=True)
            self.tick()
        elif opcode in {Opcode.ADD, Opcode.SUB, Opcode.DIV, Opcode.MOD}:
            self.get_arg(instruction)
            self.data_path.alu(opcode)
            self.tick()
            self.latch_instruction_pointer(sel_next=True)
            self.tick()
        elif opcode is Opcode.CMP:
            arg = self.get_arg(instruction)
            self.data_path.set_cmp_flags(self.data_path.acc - arg)
            self.latch_instruction_pointer(sel_next=True)
            self.tick()
        elif opcode is Opcode.JMP:
            self.latch_instruction_pointer(sel_next=False)
            self.tick()
        elif opcode is Opcode.JE:
            if self.data_path.is_zero():
                self.latch_instruction_pointer(sel_next=False)
            else:
                self.latch_instruction_pointer(sel_next=True)
            self.tick()
        elif opcode is Opcode.JNE:
            if self.data_path.is_zero():
                self.latch_instruction_pointer(sel_next=True)
            else:
                self.latch_instruction_pointer(sel_next=False)
            self.tick()
        elif opcode is Opcode.OUT:
            self.data_path.set_address_register(1)
            self.data_path.latch_data_memory()
            self.data_path.output(value_type=instruction['arg'])
            self.latch_instruction_pointer(sel_next=True)
            self.tick()
        elif opcode is Opcode.EI:
            self.interrupt_enable = True
            self.latch_instruction_pointer(sel_next=True)
            self.tick()
        elif opcode is Opcode.DI:
            self.interrupt_enable = False
            self.latch_instruction_pointer(sel_next=True)
            self.tick()
        elif opcode is Opcode.IRET:
            self.retrieve_context()
            logging.debug(f'{{STATE: {self.state.name}}}', )
        elif opcode is Opcode.IN:
            if len(self.data_path.input_buffer) == 0:
                raise EOFError
            self.data_path.input()
            self.data_path.set_address_register(0)
            self.data_path.latch_data_register()
            self.tick()
            self.data_path.latch_acc()
            self.data_path.set_flags()
            self.latch_instruction_pointer(sel_next=True)
        else:
            assert False, 'opcode not found'

    def __repr__(self):
        state = f'{{TICK: {self._tick}, IP: {self.instruction_pointer}, ADDR: {self.data_path.address_register}, ' \
                f'ACC: {self.data_path.acc}, DR: {self.data_path.data_register}, STATE: {self.state.name}}}'
        return state


def simulation(code, data_mem_size, limit, input_buffer):
    data_path = DataPath(data_mem_size, code['data'], input_buffer)
    control_unit = ControlUnit(code['text'], data_path)
    logging.debug('%s', control_unit)
    instr_counter = 0
    try:
        while True:
            assert limit > instr_counter, "too long execution, increase limit!"
            control_unit.decode_and_execute_instr()
            control_unit.check_interrupt()
            instr_counter += 1
            logging.debug('%s', control_unit)
    except EOFError:
        logging.warning('Input buffer is empty!')
    except StopIteration:
        pass
    logging.info('output_buffer: %s', repr(''.join(data_path.output_buffer)))
    return ''.join(data_path.output_buffer), instr_counter, control_unit.current_tick()

def main(args):
    assert len(args) == 2, "Wrong arguments: machine.py <code_file> <input_file>"
    code_file, input_file = args
    code = read_code(code_file)
    with open(input_file, encoding="utf-8") as file:
        input_text = file.read()
        input_token = []
        for line in input_text.split('\n'):
            line = line[1:-1].split(', ')
            tick_ch_tuple = (int(line[0]),line[1][1:-1] )
            input_token.append(tick_ch_tuple)
    output, instr_counter, ticks = simulation(code, data_mem_size=100, limit=50000, input_buffer=input_token)
    print(''.join(output))
    print("instr_counter:", instr_counter, "ticks:", ticks)


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.DEBUG)
    main(sys.argv[1:])
