# Лабораторная работа #3
Лунева Арина P33312

Вариант: `asm | acc | harv | hw | instr | struct | trap | mem | prob1`



## Описание языка

Код ассемблера может включать в себя 3 секции: .data(данные), .vector(данные вектора прерывания, при трансляции в машинный код хранятся вместе с данными из секции .data), .text(код), к ним нельзя обращаться из кода программы

Секции идут друг за другом в указанном порядке.

Секции .data и .text обязательные, .vector может отсутствовать.

Все команды должны располагаться строго внутри секций, секция кода не может быть пустой.

В языке поддерживаются метки. Меткой считается любая последовательность симолов, начинающася с точки, состоящая из латинских букв любого регистра, цифр(но не может следовать сразу после точки) и нижнего подчеркивания, в конце которой стоит двоеточие. Метки не могут повторяться.

.data:, .vector:, .text: считаются особыми метками.

В секции .data не может быть меток.

Переменные могут быть двух типов: num(число), string(строка).

```
.data:
    a num 5 
    b string 'hello'
```

Вектор прерывания должен быть проинициализирован, в случае отстувия инициализации в коде программы он будет проиниализирован значение по умолчанию.

```
.vector:
    v num .handler ; в векторе хранится адрес начала обработчика прерывания
```


В языке поддерживаются комментарии: все, что стоит после `;`, считается комментарием.



## Система команд

У операндов поддержано три режима адресации:

| мнемоника          | количество тактов                                                                             |
|:-------------------|:----------------------------------------------------------------------------------------------|
| `CONSTANT`         | операнд хранится непосредственно в команде                                                    |
| `DIRECT_ADDRESS`   | операнд - это значение, лежащее по адресу, хранящемуся в команде                              |
| `INDIRECT_ADDRESS` | операнд - это значение, лежащее по адресу, хранящемуся в ячейке, на которую указывает команда |


Пример:

```
ld #5 - в аккумулятор загружено значение 5
ld 5 - в аккумулятор загружено значение из ячейки с адресом 5
ld addr - в аккумулятор загружено значение из ячейки addr
ld (addr) - в аккумулятор загружено значение, лежащее по адресу, который хранится в ячейке addr

```


### Набор инструкций

| мнемоника | количество тактов | тип аргумента                     | описанаие   команды                                                             |
|:----------|-------------------|:----------------------------------|:------------------------------------------------------|
| `ld`      | 2-4               | `const/direct_addr/inderect_addr` | загрузить значение в acc                              |
| `st`      | 2                 | `direct_addr/inderect_addr`       | сохранить значение из acc в память                    |
| `add`     | 2-4               | `const/direct_addr/inderect_addr` | прибавить к acc аргумент                              |
| `sub`     | 2-4               | `const/direct_addr/inderect_addr` | вычесть из  acc аргумент                              |
| `jmp`     | 2                 | `direct_addr`                     | безусловный переход на аргумент-метку                 |
| `je`      | 2                 | `direct_addr`                     | переход на аргумент-метку, если равно                 |
| `jne`     | 2                 | `direct_addr`                     | переход на аргумент-метку, если не равно              |
| `div`     | 2                 | `const/direct_addr/inderect_addr` | целочисленное деление acc на аргумент                 |
| `mod`     | 2                 | `const/direct_addr/inderect_addr` | остаток от деления acc на аргумент                    |
| `inc`     | 1                 | `-`                               | значение в acc += 1                                   |
| `dec`     | 1                 | `-`                               | значение в acc -=1                                    |
| `hlt`     | 1                 | `-`                               | остановка                                             |
| `in`      | 1                 | `-`                               | чтение с утройства ввода                              |
| `out`     | 1                 | `-`                               | запись в устройство вывода                            |
| `cmp`     | 2-3               | `const/direct_addr/inderect_addr` | сравнить acc с аргументом (вычитание без записи в acc)|
| `ei`      | 1                 | `-`                               | разрешение прерываний                                 |
| `di`      | 1                 | `-`                               | запрет прерываний                                     |
| `iret`    | 2                 | `-`                               | возврат из прерывания                                 |



### Кодирование инструкций
- Машинный код сериализуется в список JSON.
- Один элемент списка, одна инструкция.
- Индекс списка -- адрес инструкции.


Пример исходного кода `hello.asm`:
```asm
.data:
    hw string "hello, world!"
    null_term num 0
    addr num 2

.text:
    .loop:
        ld (addr)
        cmp #0
        je .end
        out
        ld addr
        inc
        st addr
        jmp .loop
    .end:
        hlt

```

Результат трансляции `hello.out`:
```json
{
    "data": [
        9,
        104,
        101,
        108,
        108,
        111,
        44,
        32,
        119,
        111,
        114,
        108,
        100,
        33,
        0,
        3
    ],
    "text": [
        {
            "opcode": "ld",
            "arg": 17,
            "addressing_type": "indirect_addressing"
        },
        {
            "opcode": "cmp",
            "arg": 0,
            "addressing_type": "constant"
        },
        {
            "opcode": "je",
            "arg": 8,
            "addressing_type": "constant"
        },
        {
            "opcode": "out",
            "arg": 1,
            "addressing_type": null
        },
        {
            "opcode": "ld",
            "arg": 17,
            "addressing_type": "direct_addressing"
        },
        {
            "opcode": "inc",
            "arg": null,
            "addressing_type": null
        },
        {
            "opcode": "st",
            "arg": 17,
            "addressing_type": "direct_addressing"
        },
        {
            "opcode": "jmp",
            "arg": 0,
            "addressing_type": "constant"
        },
        {
            "opcode": "hlt",
            "arg": null,
            "addressing_type": null
        },
        {
            "opcode": "iret",
            "arg": null,
            "addressing_type": null
        }
    ]
}
```

где:

- `opcode` -- строка с кодом операции;
- `arg` -- аргумент (может отсутствовать);
- `addressing_type` -- тип адрессации.

## Организация памяти

- Память данных. Машинное слово -- 32 бита, знаковое. Реализуется списком чисел.
- Память команд. Машинное слово -- не определено. Реализуется списком словарей, описывающих инструкции (одно слово -- одна ячейка).

```text

Data memory
+-----------------------------+
| 00  : input                 |
| 01  : output                |
| 03  : interrupt vector      |
|    ...                      |
| 127 : data                  |
| 127 : program               |
| 128 : program               |
|    ...                      |
+-----------------------------+

     Instruction memory
+-----------------------------+
| 00  : instruction           |
| 01  : instruction           |
|    ...                      | 
| n   : instruction           |
|    ...                      |
+-----------------------------+

```


## Транслятор

Интерфейс командной строки: `translator.py <input_file> <target_file>"`

Реализован в модуле: [translator](./translator.py)

Каждая строка в исходном файле может быть:
- меткой
- объявлением секции
- командой
- переменной
- пустой


## Модель процессора

Реализован в модуле: [machine](./machine.py)


[Processor Scheme](./model.png)

Особенности процессора:
- Операции построены вокруг аккумулятора
- Машинное слово размером в 32 бит
- Реализована гарвардская архитектура: отдельная память для команд и данных
  - память команд:
    - `instruction_pointer` - счетчик инструкций, указывает на адрес исполняемой команды
  - память данных:
    - `address_register` - регистр адреса, по которому данные записываются в память/читаются из памяти
    - `data_register` - регистр данных, в который записываются данные из памяти/из которого записываются данные в память
- Порты ввода-вывода отображаются в память, 0 - ввод, 1 - вывод
- В ячейке с адресом 2 хранится адрес обработчика прерывания

Сигналы:
- `latch_data_memory` - защелкнуть значение в памяти
- `latch_data_register` - защелкнуть значение в регистре данных
- `latch_acc` - защелкнуть значение в аккумуляторе
- `get_arg` - выбор аргумента в зависимости от адресации


Журнал работы виртуальной машины для программы `hello.out`:
```
C:\Users\Arina\Desktop\labs\3\ак\lab3\venv\Scripts\python.exe C:\Users\Arina\Desktop\labs\3\ак\lab3\machine.py hello.out C:\Users\Arina\Desktop\labs\3\ак\lab3\examples\hello_input.txt 
DEBUG:root:{TICK: 0, IP: 0, ADDR: 0, ACC: 0, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 3, IP: 1, ADDR: 3, ACC: 104, DR: 104, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.CMP started}
DEBUG:root:{TICK: 4, IP: 2, ADDR: 3, ACC: 104, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JE started}
DEBUG:root:{TICK: 5, IP: 3, ADDR: 3, ACC: 104, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.OUT started}
DEBUG:root:output: '' << 'h'
DEBUG:root:{TICK: 6, IP: 4, ADDR: 1, ACC: 104, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 8, IP: 5, ADDR: 17, ACC: 3, DR: 3, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.INC started}
DEBUG:root:{TICK: 9, IP: 6, ADDR: 17, ACC: 4, DR: 3, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.ST started}
DEBUG:root:{TICK: 10, IP: 7, ADDR: 17, ACC: 4, DR: 3, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JMP started}
DEBUG:root:{TICK: 11, IP: 0, ADDR: 17, ACC: 4, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 14, IP: 1, ADDR: 4, ACC: 101, DR: 101, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.CMP started}
DEBUG:root:{TICK: 15, IP: 2, ADDR: 4, ACC: 101, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JE started}
DEBUG:root:{TICK: 16, IP: 3, ADDR: 4, ACC: 101, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.OUT started}
DEBUG:root:output: 'h' << 'e'
DEBUG:root:{TICK: 17, IP: 4, ADDR: 1, ACC: 101, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 19, IP: 5, ADDR: 17, ACC: 4, DR: 4, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.INC started}
DEBUG:root:{TICK: 20, IP: 6, ADDR: 17, ACC: 5, DR: 4, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.ST started}
DEBUG:root:{TICK: 21, IP: 7, ADDR: 17, ACC: 5, DR: 4, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JMP started}
DEBUG:root:{TICK: 22, IP: 0, ADDR: 17, ACC: 5, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 25, IP: 1, ADDR: 5, ACC: 108, DR: 108, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.CMP started}
DEBUG:root:{TICK: 26, IP: 2, ADDR: 5, ACC: 108, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JE started}
DEBUG:root:{TICK: 27, IP: 3, ADDR: 5, ACC: 108, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.OUT started}
DEBUG:root:output: 'he' << 'l'
DEBUG:root:{TICK: 28, IP: 4, ADDR: 1, ACC: 108, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 30, IP: 5, ADDR: 17, ACC: 5, DR: 5, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.INC started}
DEBUG:root:{TICK: 31, IP: 6, ADDR: 17, ACC: 6, DR: 5, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.ST started}
DEBUG:root:{TICK: 32, IP: 7, ADDR: 17, ACC: 6, DR: 5, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JMP started}
DEBUG:root:{TICK: 33, IP: 0, ADDR: 17, ACC: 6, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 36, IP: 1, ADDR: 6, ACC: 108, DR: 108, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.CMP started}
DEBUG:root:{TICK: 37, IP: 2, ADDR: 6, ACC: 108, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JE started}
DEBUG:root:{TICK: 38, IP: 3, ADDR: 6, ACC: 108, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.OUT started}
DEBUG:root:output: 'hel' << 'l'
DEBUG:root:{TICK: 39, IP: 4, ADDR: 1, ACC: 108, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 41, IP: 5, ADDR: 17, ACC: 6, DR: 6, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.INC started}
DEBUG:root:{TICK: 42, IP: 6, ADDR: 17, ACC: 7, DR: 6, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.ST started}
DEBUG:root:{TICK: 43, IP: 7, ADDR: 17, ACC: 7, DR: 6, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JMP started}
DEBUG:root:{TICK: 44, IP: 0, ADDR: 17, ACC: 7, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 47, IP: 1, ADDR: 7, ACC: 111, DR: 111, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.CMP started}
DEBUG:root:{TICK: 48, IP: 2, ADDR: 7, ACC: 111, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JE started}
DEBUG:root:{TICK: 49, IP: 3, ADDR: 7, ACC: 111, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.OUT started}
DEBUG:root:output: 'hell' << 'o'
DEBUG:root:{TICK: 50, IP: 4, ADDR: 1, ACC: 111, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 52, IP: 5, ADDR: 17, ACC: 7, DR: 7, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.INC started}
DEBUG:root:{TICK: 53, IP: 6, ADDR: 17, ACC: 8, DR: 7, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.ST started}
DEBUG:root:{TICK: 54, IP: 7, ADDR: 17, ACC: 8, DR: 7, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JMP started}
DEBUG:root:{TICK: 55, IP: 0, ADDR: 17, ACC: 8, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 58, IP: 1, ADDR: 8, ACC: 44, DR: 44, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.CMP started}
DEBUG:root:{TICK: 59, IP: 2, ADDR: 8, ACC: 44, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JE started}
DEBUG:root:{TICK: 60, IP: 3, ADDR: 8, ACC: 44, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.OUT started}
DEBUG:root:output: 'hello' << ','
DEBUG:root:{TICK: 61, IP: 4, ADDR: 1, ACC: 44, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 63, IP: 5, ADDR: 17, ACC: 8, DR: 8, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.INC started}
DEBUG:root:{TICK: 64, IP: 6, ADDR: 17, ACC: 9, DR: 8, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.ST started}
DEBUG:root:{TICK: 65, IP: 7, ADDR: 17, ACC: 9, DR: 8, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JMP started}
DEBUG:root:{TICK: 66, IP: 0, ADDR: 17, ACC: 9, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 69, IP: 1, ADDR: 9, ACC: 32, DR: 32, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.CMP started}
DEBUG:root:{TICK: 70, IP: 2, ADDR: 9, ACC: 32, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JE started}
DEBUG:root:{TICK: 71, IP: 3, ADDR: 9, ACC: 32, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.OUT started}
DEBUG:root:output: 'hello,' << ' '
DEBUG:root:{TICK: 72, IP: 4, ADDR: 1, ACC: 32, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 74, IP: 5, ADDR: 17, ACC: 9, DR: 9, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.INC started}
DEBUG:root:{TICK: 75, IP: 6, ADDR: 17, ACC: 10, DR: 9, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.ST started}
DEBUG:root:{TICK: 76, IP: 7, ADDR: 17, ACC: 10, DR: 9, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JMP started}
DEBUG:root:{TICK: 77, IP: 0, ADDR: 17, ACC: 10, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 80, IP: 1, ADDR: 10, ACC: 119, DR: 119, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.CMP started}
DEBUG:root:{TICK: 81, IP: 2, ADDR: 10, ACC: 119, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JE started}
DEBUG:root:{TICK: 82, IP: 3, ADDR: 10, ACC: 119, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.OUT started}
DEBUG:root:output: 'hello, ' << 'w'
DEBUG:root:{TICK: 83, IP: 4, ADDR: 1, ACC: 119, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 85, IP: 5, ADDR: 17, ACC: 10, DR: 10, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.INC started}
DEBUG:root:{TICK: 86, IP: 6, ADDR: 17, ACC: 11, DR: 10, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.ST started}
DEBUG:root:{TICK: 87, IP: 7, ADDR: 17, ACC: 11, DR: 10, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JMP started}
DEBUG:root:{TICK: 88, IP: 0, ADDR: 17, ACC: 11, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 91, IP: 1, ADDR: 11, ACC: 111, DR: 111, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.CMP started}
DEBUG:root:{TICK: 92, IP: 2, ADDR: 11, ACC: 111, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JE started}
DEBUG:root:{TICK: 93, IP: 3, ADDR: 11, ACC: 111, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.OUT started}
DEBUG:root:output: 'hello, w' << 'o'
DEBUG:root:{TICK: 94, IP: 4, ADDR: 1, ACC: 111, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 96, IP: 5, ADDR: 17, ACC: 11, DR: 11, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.INC started}
DEBUG:root:{TICK: 97, IP: 6, ADDR: 17, ACC: 12, DR: 11, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.ST started}
DEBUG:root:{TICK: 98, IP: 7, ADDR: 17, ACC: 12, DR: 11, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JMP started}
DEBUG:root:{TICK: 99, IP: 0, ADDR: 17, ACC: 12, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 102, IP: 1, ADDR: 12, ACC: 114, DR: 114, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.CMP started}
DEBUG:root:{TICK: 103, IP: 2, ADDR: 12, ACC: 114, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JE started}
DEBUG:root:{TICK: 104, IP: 3, ADDR: 12, ACC: 114, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.OUT started}
DEBUG:root:output: 'hello, wo' << 'r'
DEBUG:root:{TICK: 105, IP: 4, ADDR: 1, ACC: 114, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 107, IP: 5, ADDR: 17, ACC: 12, DR: 12, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.INC started}
DEBUG:root:{TICK: 108, IP: 6, ADDR: 17, ACC: 13, DR: 12, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.ST started}
DEBUG:root:{TICK: 109, IP: 7, ADDR: 17, ACC: 13, DR: 12, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JMP started}
DEBUG:root:{TICK: 110, IP: 0, ADDR: 17, ACC: 13, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 113, IP: 1, ADDR: 13, ACC: 108, DR: 108, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.CMP started}
DEBUG:root:{TICK: 114, IP: 2, ADDR: 13, ACC: 108, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JE started}
DEBUG:root:{TICK: 115, IP: 3, ADDR: 13, ACC: 108, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.OUT started}
DEBUG:root:output: 'hello, wor' << 'l'
DEBUG:root:{TICK: 116, IP: 4, ADDR: 1, ACC: 108, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 118, IP: 5, ADDR: 17, ACC: 13, DR: 13, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.INC started}
DEBUG:root:{TICK: 119, IP: 6, ADDR: 17, ACC: 14, DR: 13, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.ST started}
DEBUG:root:{TICK: 120, IP: 7, ADDR: 17, ACC: 14, DR: 13, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JMP started}
DEBUG:root:{TICK: 121, IP: 0, ADDR: 17, ACC: 14, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 124, IP: 1, ADDR: 14, ACC: 100, DR: 100, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.CMP started}
DEBUG:root:{TICK: 125, IP: 2, ADDR: 14, ACC: 100, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JE started}
DEBUG:root:{TICK: 126, IP: 3, ADDR: 14, ACC: 100, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.OUT started}
DEBUG:root:output: 'hello, worl' << 'd'
DEBUG:root:{TICK: 127, IP: 4, ADDR: 1, ACC: 100, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 129, IP: 5, ADDR: 17, ACC: 14, DR: 14, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.INC started}
DEBUG:root:{TICK: 130, IP: 6, ADDR: 17, ACC: 15, DR: 14, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.ST started}
DEBUG:root:{TICK: 131, IP: 7, ADDR: 17, ACC: 15, DR: 14, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JMP started}
DEBUG:root:{TICK: 132, IP: 0, ADDR: 17, ACC: 15, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 135, IP: 1, ADDR: 15, ACC: 33, DR: 33, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.CMP started}
DEBUG:root:{TICK: 136, IP: 2, ADDR: 15, ACC: 33, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JE started}
DEBUG:root:{TICK: 137, IP: 3, ADDR: 15, ACC: 33, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.OUT started}
DEBUG:root:output: 'hello, world' << '!'
DEBUG:root:{TICK: 138, IP: 4, ADDR: 1, ACC: 33, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 140, IP: 5, ADDR: 17, ACC: 15, DR: 15, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.INC started}
DEBUG:root:{TICK: 141, IP: 6, ADDR: 17, ACC: 16, DR: 15, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.ST started}
DEBUG:root:{TICK: 142, IP: 7, ADDR: 17, ACC: 16, DR: 15, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JMP started}
DEBUG:root:{TICK: 143, IP: 0, ADDR: 17, ACC: 16, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.LD started}
DEBUG:root:{TICK: 146, IP: 1, ADDR: 16, ACC: 0, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.CMP started}
DEBUG:root:{TICK: 147, IP: 2, ADDR: 16, ACC: 0, DR: 0, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.JE started}
DEBUG:root:{TICK: 148, IP: 8, ADDR: 16, ACC: 0, DR: 8, STATE: MAIN_PROGRAM}
DEBUG:root:{Opcode.HLT started}
INFO:root:output_buffer: 'hello, world!'
hello, world!
instr_counter: 107 ticks: 148

```


## Апробация

Вариант: `asm | acc | harv | hw | instr | struct | trap | mem | prob1`

| ФИО           | Алгоритм | LoC | code байт | code инстр | инстр. | такт  |
|---------------|----------|-----|-----------|------------|--------|-------|
| Лунева Арина  | hello    | 16  | -         | 26         | 107    | 148   |
| Лунева Арина  | cat      | 16  | -         | 10         | 39     | 46    |
| Лунева Арина  | prob1    | 32  | -         | 28         | 14590  | 22984 |

